import sys, os, json
from utils import *

# Constants
APP_PATH = os.path.dirname(os.path.realpath(__file__))+"/"
STORY_PATH = APP_PATH+"stories/"
sys.path.append(APP_PATH)
import dearpygui.dearpygui as dpg
VIEWPORT_WIDTH = 1200
VIEWPORT_HEIGHT = 700
WINDOW_TITLE = "Tyr ($)"
SCENE_TITLE = "Scene Editor ($)"
NEW_TOKEN = "-- NEW --"

# Init
if not os.path.exists(STORY_PATH):
    os.mkdir(STORY_PATH)
dpg.create_context()
dpg.configure_app(init_file=APP_PATH+"windows.ini")
dpg.create_viewport(title='Tyr', width=VIEWPORT_WIDTH, height=VIEWPORT_HEIGHT)
dpg.setup_dearpygui()


# State globals
last_clicked_ent_type = None
last_clicked_entity = None
last_clicked_chapter = None
last_clicked_scene = None
story_state = {}
open_filename = None

def default_story_state():
    global story_state
    global last_clicked_chapter, last_clicked_ent_type, last_clicked_entity, last_clicked_scene
    global open_filename
    dpg.set_viewport_title("Tyr")
    last_clicked_chapter = None
    last_clicked_ent_type = None
    last_clicked_entity = None
    last_clicked_scene = None
    open_filename = None
    dpg.set_value("story_editor", "")
    dpg.set_value("code_editor_ent", "")
    story_state = {
        "chapters": {
            "1": {
                "example": {"text": "# Chapter 1", "metadata": {"include": True}}
            }
        },
        "entities": {
            "characters": {
                "example": {"text": "This is an example entity.", "metadata": {}}
            },
            "places": {}
        }
    }
    update_from_state()

def update_from_state():
    clear_listbox("ent_list")
    clear_listbox("ent_type_list")
    clear_listbox("chapter_list")
    clear_listbox("scene_list")
    append_listbox("ent_type_list", NEW_TOKEN)
    append_listbox("ent_list", NEW_TOKEN)
    append_listbox("chapter_list", NEW_TOKEN)
    append_listbox("scene_list", NEW_TOKEN)

    for chapter in story_state["chapters"]:
        append_listbox("chapter_list", chapter)

    for etype in story_state["entities"]:
        append_listbox("ent_type_list", etype)

def open_load_ui():
    def do_load(w, name):
        load(STORY_PATH+name)
        fully_destroy("load_ui")

    with dpg.window(tag="load_ui", modal=True, width=200):
        dpg.add_listbox(tag="stories", callback=do_load, num_items=18)
        for filename in os.listdir(STORY_PATH):
            append_listbox("stories", filename.split(".")[0])

def open_save_ui():
    def do_save():
        global open_filename
        name = dpg.get_value("filename")
        open_filename = name
        save_as(STORY_PATH+name)
        fully_destroy("save_ui")

    with dpg.window(tag="save_ui", modal=True, width=200):
        with dpg.group(horizontal=True):
            dpg.add_input_text(tag="filename", hint="Story name")
            dpg.add_button(label="Save", tag="savefile", callback=lambda: do_save())

        dpg.focus_item("filename")

def load(path):
    global story_state, open_filename
    if not path.endswith(".json"):
        path = path+".json"

    with open(path, 'r') as f:
        story_state = json.load(f)
    open_filename = os.path.basename(path)[:-5]
    dpg.set_viewport_title(WINDOW_TITLE.replace("$", open_filename))
    update_from_state()


def save_as(path):
    global story_state, open_filename
    if not path.endswith(".json"):
        path = path+".json"

    dpg.save_init_file(APP_PATH+"windows.ini")
    with open(path, 'w+') as f:
        json.dump(story_state, f)
    open_filename = os.path.basename(path)[:-5]
    dpg.set_viewport_title(WINDOW_TITLE.replace("$", open_filename))

def save():
    global open_filename
    if open_filename != None:
        save_as(STORY_PATH+open_filename)
    else:
        open_save_ui()

def open_save_ui():
    def do_save():
        name = dpg.get_value("filename")
        save_as(STORY_PATH+name)
        fully_destroy("save_ui")

    with dpg.window(tag="save_ui", modal=True, width=200):
        with dpg.group(horizontal=True):
            dpg.add_input_text(tag="filename", hint="Story name")
            dpg.add_button(label="Save", tag="savefile", callback=lambda: do_save())

def add_entity_ui():
    global story_state
    if last_clicked_ent_type != None:
        def do_it():
            name = dpg.get_value("entname")
            story_state["entities"][last_clicked_ent_type][name] = {"text": "", "metadata": {}}
            append_listbox("ent_list", name)
            fully_destroy("add_ent_input")

        with dpg.window(tag="add_ent_input", modal=True, width=200):
            with dpg.group(horizontal=True):
                dpg.add_input_text(tag="entname", hint="Entity ID", callback=lambda: do_it(), on_enter=True)
                dpg.add_button(label="Save", callback=lambda: do_it())
                dpg.focus_item("entname")

def add_entity_type_ui():
    global story_state
    def do_it():
        name = dpg.get_value("enttypename")
        story_state["entities"][name] = {}
        append_listbox("ent_type_list", name)
        fully_destroy("add_enttype_input")

    with dpg.window(tag="add_enttype_input", modal=True, width=200):
        with dpg.group(horizontal=True):
            dpg.add_input_text(tag="enttypename", hint="Entity type", callback=lambda: do_it(), on_enter=True)
            dpg.add_button(label="Save", callback=lambda: do_it())
            dpg.focus_item("enttypename")

def add_chapter_ui():
    global story_state
    def do_it():
        name = str(dpg.get_value("chapname"))
        story_state["chapters"][name] = {}
        append_listbox("chapter_list", name)
        fully_destroy("add_chap_input")

    with dpg.window(tag="add_chap_input", modal=True, width=200):
        with dpg.group(horizontal=True):
            dpg.add_input_int(tag="chapname")
            dpg.add_button(label="Save", callback=lambda: do_it())
            dpg.focus_item("chapname")

def add_scene_ui():
    global last_clicked_ent_type, last_clicked_chapter, story_state
    if last_clicked_chapter != None:
        def do_it():
            name = dpg.get_value("scenename")
            story_state["chapters"][last_clicked_chapter][name] = {"text": f"# Chapter {last_clicked_chapter}; {name}", "metadata": {"include": True}}
            append_listbox("scene_list", name)
            fully_destroy("add_scene_input")

        with dpg.window(tag="add_scene_input", modal=True, width=200):
            with dpg.group(horizontal=True):
                dpg.add_input_text(tag="scenename", hint="Scene name", callback=lambda: do_it(), on_enter=True)
                dpg.add_button(label="Save", callback=lambda: do_it())
                dpg.focus_item("scenename")

def clicked_obj(w, item):
    global last_clicked_ent_type, last_clicked_chapter, last_clicked_entity, last_clicked_scene
    global story_state
    if w == "ent_type_list":
        if item == NEW_TOKEN:
            add_entity_type_ui()
            return
            
        last_clicked_ent_type = item
        clear_listbox("ent_list")
        append_listbox("ent_list", NEW_TOKEN)
        for ent in story_state["entities"][item]:
            append_listbox("ent_list", ent)

    if w == "ent_list":
        if item == NEW_TOKEN:
            add_entity_ui()
            return

        if last_clicked_ent_type != None:
            last_clicked_entity = item
            dpg.set_value("code_editor_ent",story_state["entities"][last_clicked_ent_type][item]['text'])



    if w == "chapter_list":
        if item == NEW_TOKEN:
            add_chapter_ui()
            return

        last_clicked_chapter = item
        clear_listbox("scene_list")
        append_listbox("scene_list", NEW_TOKEN)
        for ent in story_state["chapters"][item]:
            append_listbox("scene_list", ent)

    if w == "scene_list":
        if item == NEW_TOKEN:
            add_scene_ui()
            return

        if last_clicked_chapter != None:
            last_clicked_scene = item
            #print(dpg.get_item_configuration("story_editor"))
            dpg.configure_item("story_browserw", label=SCENE_TITLE.replace("$", item))
            dpg.set_value("story_editor",story_state["chapters"][last_clicked_chapter][item]['text'])


def saving_entity_editor(w, i):
    global story_state
    if last_clicked_ent_type != None and last_clicked_entity != None:
        story_state["entities"][last_clicked_ent_type][last_clicked_entity]["text"] = dpg.get_value("code_editor_ent")

def saving_story_editor(w, i):
    global story_state
    if last_clicked_chapter != None and last_clicked_scene != None:
        story_state["chapters"][last_clicked_chapter][last_clicked_scene]["text"] = dpg.get_value("story_editor")

with dpg.window(tag="ent_browser", label="Entity Browser", no_close=True, width=250):        
    with dpg.group(horizontal=True):
        dpg.add_listbox(tag="ent_type_list", callback=clicked_obj, width=100, num_items=18)
        dpg.add_listbox(tag="ent_list", callback=clicked_obj, width=100, num_items=18)
        append_listbox("ent_type_list", NEW_TOKEN)
        append_listbox("ent_list", NEW_TOKEN)
        dpg.add_input_text(tag="code_editor_ent", multiline=True, width=250, height=200, callback=saving_entity_editor)

with dpg.window(tag="story_browser", label="Story Browser", no_close=True, width=250):        
    with dpg.group(horizontal=True):
        dpg.add_listbox(tag="chapter_list", callback=clicked_obj, width=100, num_items=18)
        dpg.add_listbox(tag="scene_list", callback=clicked_obj, width=100, num_items=18)
        append_listbox("chapter_list", NEW_TOKEN)
        append_listbox("scene_list", NEW_TOKEN)

with dpg.window(tag="story_browserw", label="Scene Editor", no_close=True, width=250): 
    dpg.add_input_text(tag="story_editor", multiline=True, width=500, height=600, callback=saving_story_editor)

with dpg.viewport_menu_bar():
    with dpg.menu(label="File", tag="menu_file"):
        dpg.add_menu_item(label="New", callback=lambda: default_story_state())
        dpg.add_menu_item(label="Load", callback=lambda: open_load_ui())
        dpg.add_menu_item(label="Save", callback=lambda: save())
        dpg.add_menu_item(label="Save as...", callback=lambda: open_save_ui())
        dpg.add_separator()
        dpg.add_menu_item(label="Export")
    with dpg.menu(label="Operations", tag="menu_ops"):
        dpg.add_menu_item(label="New entity group")
        dpg.add_menu_item(label="New entity")
        dpg.add_menu_item(label="New chapter")
        dpg.add_menu_item(label="New scene")

HELP_D = {
    "Main": "This is the main help heading.",
    "Keyboard": """ = Keyboard = 
F1 = Help
F2 = Evaluator
F3 = Focus on terminal
    """
}

def open_help(subheading = None):
    with dpg.window(label = "Help", tag="help menu", on_close=lambda: fully_destroy("help menu")):
        with dpg.group(parent="help menu", horizontal=True):
            dpg.add_group(tag="help_nav")
            dpg.add_input_text(tag="help_text", width=300, height=400, multiline=True)

        def set_help(w):
            lbl = dpg.get_item_label(w)
            dpg.set_value("help_text", HELP_D[lbl])

        for heading, body in HELP_D.items():
            dpg.add_button(label = heading, parent="help_nav", callback=set_help, width=100)

ctrl_mod = False
alt_mod = False
def handle_key_input(w, key):
    global alt_mod, ctrl_mod
    #print(w, key)
    if key == 290:
        if dpg.does_item_exist("help menu"):
            dpg.focus_item("help menu")
        else:
            open_help()
    if key == 291:
        pass #new
    if key == 292:
        pass #load
    if key == 293:
        pass #save
    if key == 294:
        pass #save
    if key == 257:
        if dpg.does_item_exist("save_ui"):
            name = dpg.get_value("filename")
            save_as(STORY_PATH+name)
            fully_destroy("save_ui")
    if key == 341:
        ctrl_mod = True

    if key == 83 and ctrl_mod:
        save()

def handle_keyr_input(w, key):
    global alt_mod, ctrl_mod
    if key == 341:
        ctrl_mod = False

with dpg.handler_registry(): 
    dpg.add_key_press_handler(callback=handle_key_input)
    dpg.add_key_release_handler(callback=handle_keyr_input)

if __name__ == "__main__":
    dpg.show_viewport()
    default_story_state()
    update_from_state()
    dpg.start_dearpygui()
    dpg.destroy_context()